# frozen_string_literal: true

require 'json'
require 'intercom'
require 'csv'

class ConvoParser
  attr_reader :intercom, :output_file

  def initialize(client, file_name)
    @intercom = client
    @output_file = file_name

    File.write(file_name, '')
  end

  def write_to_csv(content)
    File.open(output_file, 'a+') do |f|
      f.puts("#{content}\n")
    end
  end

  def parse_conversation_part(convoid,convo_part)
    unless convo_part.body.nil?
      text=convo_part.body.gsub(/\R+/, "")
      write_to_csv("#{convoid}|#{convo_part.id}|#{convo_part.author}|#{convo_part.part_type}|\"#{text}\"|#{ARGV.first}")
    else
      write_to_csv("#{convoid}|#{convo_part.id}|#{convo_part.author}|#{convo_part.part_type}|\"#{convo_part.body}\"|#{ARGV.first}")
    end
  end

  def parse_conversation_parts(convo)
    convoid=convo.id.to_s
    write_to_csv(convoid)
    convo.conversation_parts.each do |convo_part|
      parse_conversation_part(convoid,convo_part)
    end
  end

  def check_rate_limit
    current_rate = yield
    # write_to_csv("RATE LIMIT: #{intercom.rate_limit_details[:remaining]}")
    if intercom.rate_limit_details[:remaining] < 30
      sleep 10
      write_to_csv('SLEEPING')
    end
    current_rate
  end
end

class ConvoSetup
  attr_reader :intercom, :convo_parser

  def initialize(_ACCESS_TOKEN, file_name)
    @intercom = Intercom::Client.new(token: ENV['ACCESS_TOKEN'])
    @convo_parser = ConvoParser.new(intercom, file_name)
  end

  def get_single_convo(convo_id)
    # Get a single conversation
    convo_parser.check_rate_limit do
      intercom.conversations.find(id: convo_id)
    end
  end

  def run
    if ARGV.empty?
      puts('Error: NO Valid iD PROVIDED - Please provide the id of the admin you want to export the conversations')
      exit
    end
    convo_parser.write_to_csv('CONVO ID|Message ID|AUTHOR|PART TYPE|PART BODY|AGENT ID')
    puts("Exporting all conversations created after #{ARGV[1]}/#{ARGV[2]}/#{ARGV[3]} and assigned to admin with id: #{ARGV[0]}")
    count = 0
    result = intercom.conversations.find_all(type: 'admin', id: ARGV[0])
    intercom.conversations.find_all(type: 'admin', id: ARGV[0]).each do |convo|
      if convo.created_at <= Time.new(ARGV[1], ARGV[2], ARGV[3])
        puts('Exporting conversations...') if count.even?
      else
        convo_parser.parse_conversation_parts(get_single_convo(convo.id))
        count += 1
      end
    end
    puts("#{count} conversations exported")
  end
end

ConvoSetup.new('AT', "all_convos_created_after_#{ARGV[1]}#{ARGV[2]}#{ARGV[2]}_assigned_to_#{ARGV[0]}.csv").run
