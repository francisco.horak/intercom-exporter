# frozen_string_literal: true

require_relative "app/version"

module Intercom
  module Export
    module Ruby
      module App
        class Error < StandardError; end
        # Your code goes here...
      end
    end
  end
end
