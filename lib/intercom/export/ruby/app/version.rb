# frozen_string_literal: true

module Intercom
  module Export
    module Ruby
      module App
        VERSION = "0.1.0"
      end
    end
  end
end
