# frozen_string_literal: true

require 'json'
require 'intercom'
require 'csv'

class ConvoParser
  attr_reader :intercom, :output_file

  def initialize(client, file_name)
    @intercom = client
    @output_file = file_name

    File.write(file_name, '')
  end

  def write_to_csv(content)
    File.open(output_file, 'a+') do |f|
      f.puts("#{content}\n")
    end
  end

  def parse_conversation_part(convoid,convo_part)
    unless convo_part.body.nil?
      text=convo_part.body.gsub(/\R+/, "")
      write_to_csv("#{convoid}|#{convo_part.id}|#{convo_part.author}|#{convo_part.part_type}|\"#{text}\"")
    else
      write_to_csv("#{convoid}|#{convo_part.id}|#{convo_part.author}|#{convo_part.part_type}|\"#{convo_part.body}\"")
    end
  end

  def parse_conversation_parts(convo)
    total_count = convo.conversation_parts.length
    current_count = 0
    convoid=convo.id.to_s
    write_to_csv(convoid)
    convo.conversation_parts.each do |convo_part|
      parse_conversation_part(convoid,convo_part)
    end
  end

  def check_rate_limit
    current_rate = yield
    # write_to_csv("RATE LIMIT: #{intercom.rate_limit_details[:remaining]}")
    if intercom.rate_limit_details[:remaining] < 30
      sleep 10
      write_to_csv('SLEEPING')
    end
    current_rate
  end
end

class ConvoSetup
  attr_reader :intercom, :convo_parser

  def initialize(_ACCESS_TOKEN, file_name)
    @intercom = Intercom::Client.new(token: ENV['ACCESS_TOKEN'])
    @convo_parser = ConvoParser.new(intercom, file_name)
  end

  def get_single_convo(convo_id)
    # Get a single conversation
    convo_parser.check_rate_limit do
      intercom.conversations.find(id: convo_id)
    end
  end

  def get_first_page_of_conversations
    # Get the first page of your conversations
    convo_parser.check_rate_limit do
      convos = intercom.get('/conversations', '')
      convos
    end
  end

  def get_next_page_of_conversations(next_page_url)
    intercom.get(next_page_url, '')
  end

  def run
    convo_parser.write_to_csv('CONVO ID|PART ID|AUTHOR|PART TYPE|PART BODY')
    puts('Preparing to export all conversations....')
    result = get_first_page_of_conversations
    current_page = 1
    count = 1
    messages = 0
    total = result['pages']['per_page'] * result['pages']['total_pages']

    until current_page.nil?
      # Parse through each conversation to see what is provided via the list
      result['conversations'].each do |single_convo|
        puts "Exporting conversation #{count} of #{total}"
        convo_parser.parse_conversation_parts(get_single_convo(single_convo['id']))
        count += 1
        messages += single_convo['statistics']['count_conversation_parts']
        puts("Total of #{messages} lines/conversation parts exported so far")
      end
      puts "PAGINATION: page #{result['pages']['page']} of #{result['pages']['total_pages']}"
      current_page = result['pages']['next']
      if current_page.nil?
        # Dont make any more calls since we are on the last page
        break
      end

      result = get_next_page_of_conversations(result['pages']['next'])
    end
  end
end

ConvoSetup.new('AT', 'all_chat_transcripts.csv').run
