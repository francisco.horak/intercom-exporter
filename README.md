# Intercom Data Exporter

This app allows Intercom Users to export chat transcripts (Conversations) in bulk to a csv file. It's built with the Ruby SDK from Intercom and allows you to either export all conversations or filter conversations based on a team member. 


## Features

- Extract all conversations;
- Extract all conversations assigned to a specific Admin;
- [SOON] Extract chat transcripts by status 



## Usage

Clone this Repo to your local envoirnment.

```zsh
$ git clone <url>
```
To install the dependencies, run:

```zsh
$ cd intercom-Exporter
$ bundle install
```
## Environment Variables

To run this app, you will need to have an API token from Intercom. You can find more information on the [documetation from Intercom.](https://developers.intercom.com/building-apps/docs/authentication-types) 

Once you have cloned/installed the package, you will need to set an Environment variable `ACCESS_TOKEN` with your Intercom Token.

To configure your `ENV` variable on ruby run the following ruby command:

```zsh
$ export ACCESS_TOKEN=your-intercom-token
```


## Running the Scripts
There are two available export options:

- Extracting all conversations for all admins (All time) 
- Extracting all conversations that were assigned to a given team member (by ID and date)

In order to export all conversations, run the following command on your terminal:

```zsh
$ ruby export_all_conversations.rb
```

In order to export conversations created after a given date that were assigned to a specific team member, run the following command on your terminal: 

```zsh
$ ruby export_by_admin_and_date.rb <adminID> <year> <month[optional]> <day[optional]>

// Year is required, month and day are optional.
// Ex: Exporting all conversations created after 2022 and assigned to Admin with ID "2704890"
// $ ruby export_by_admin_and_date.rb 2704890 2022
```

#### Note: You will need to run this script for each admin at a time, if you intend to get transcripts for more than one admin. This also means that it will generate one file per Admin. 

## Output:

```ruby
CONVO ID|PART ID|AUTHOR|PART TYPE|PART BODY|AGENT ID
85500600000092|14325446794|<Intercom::Admin:0x00007fb29b1cb088>|comment|"<p>Hello Agent</p>"|203029

```

## Roadmap

- Publish GEM
- ID input Validation
- Allow for multiple IDs at once
- Allow filtering for conversation state

## Feedback
If you have any feedback or need support, please reach out to francisco.horak@ubabel.com